let menu = {
	whiteLine: document.querySelector('.white_line'),
	
	home: document.querySelector('.home'),

	moving: function(e) {
		let eLeft = e.target.offsetLeft;
		let eWidth = e.target.offsetWidth;
		menu.whiteLine.style.left = eLeft + "px";
		menu.whiteLine.style.width = eWidth + "px";
	}
}

let menuList = document.querySelectorAll('.menu_list');
for (let i = 0; i < menuList.length; i++) {
	(function(i) {
		menuList[i].addEventListener('mouseover', menu.moving)
	})(i);
};


var mobileMenuBtn = document.querySelector('.mobileMenuBtn');
var mobileMenu = document.querySelector('.mobileMenu');

mobileMenuBtn.addEventListener('click', function() {
    if (mobileMenu.style.top < 0 + 'px') {
        mobileMenu.style.top = 40 + 'px';
    } else {
        mobileMenu.style.top = -250 + 'px';        
    };
})

// window.addEventListener('click', function() {
//     if (mobileMenu.style.top != -250 + 'px') {
//         mobileMenu.style.top = -250 + 'px';
//     };
// })
